%define debug_package %{nil}
%define _collectd /etc/collectd.d

Name: scouting-collectd-config-scoutdaq
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting RPM to install the collectd config for the scoutdaq machines
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: none
ExclusiveOs: linux
Provides: scouting-collectd-config-scoutdaq
Requires: collectd, collectd-write_prometheus

%description
collectd config file installation for CMS L1 Scouting scoutdaq machines.

%files
%attr(640, root, root) %config %{_collectd}/collectd-scoutdaq.conf

%prep

%build

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_collectd}
pwd
cp %{_basedir}/collectd/scoutdaq/collectd-scoutdaq.conf $RPM_BUILD_ROOT%{_collectd}

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :

%pre

%post
systemctl restart collectd

%preun

%postun
# Only for uninstall!
if [ $1 -eq 0 ] ; then
  rm -f %{_collectd}/collectd-scoutdaq.conf
  systemctl restart collectd
fi
