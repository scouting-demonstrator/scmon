#!/bin/bash

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BUILD_DIR="${DIR}/scouting-collectd-config-scoutdaq_rpmbuild"
cd "$DIR" || exit
echo "Directory $DIR"

RAWVER=$(git describe --tags --long --match "collectd-scoutdaq-v*" || echo 'collectd-scoutdaq-v0.0.0') # If git describe fails, we set a default version number.
VVER=$(echo "${RAWVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[3]}')
VER=$(echo "${VVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}')
REL=$(echo "${RAWVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[4]}')
TIM="$(date -u +"%F %T %Z")"
LOGINNAME="$(logname)"
PCR=${LOGINNAME:-${GITLAB_USER_LOGIN}_CI}

echo -n "{" >info.json
echo -n "\"version\": \"$VER\", " >>info.json
echo -n "\"release\": \"$REL\", " >>info.json
echo -n "\"time\": \"$TIM\", " >>info.json
echo -n "\"packager\": \"$PCR\"" >>info.json
echo -n "}" >>info.json

echo "Version $VER, release $REL"

RPM_OPTS=(--define "_basedir $DIR" --define "_topdir $BUILD_DIR" --define "_version $VER" --define "_release $REL" --define "_packager $PCR")
rpmbuild "${RPM_OPTS[@]}" -bb package/package-scoutdaq_collectd.spec
cp "${BUILD_DIR}"/RPMS/x86_64/scouting-collectd-config-scoutdaq-* .
